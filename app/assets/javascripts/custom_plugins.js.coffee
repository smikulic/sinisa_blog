# ========================================
#  1. Tabs
#
# WARNING: You can't have tabs within tabs
#
# Manual: You need three things to make this work:
# a container, .tab-unit(s), and .tab-link(s). Both links and units
# need to be wrapped with a container on which you call this method.
# You can optionally pass in a tab link that will be
# selected by default as an argument.
# If not, the first tab link will be selected by default.
# If you want the tabs to operate on hover instead of click,
# pass in hover: true to settings object
#
# Active link gets class="active-tab-link"
# Active tab gets class="active-tab"
#
# Example:
#
# <div class="tabs-container">
# <a href="#tab1" class="tab-link">Load Tab1</a>
# <a href="#tab2" class="tab-link id="default-tab">Load Tab2</a>
#
# <div class="tab-unit" id="tab1"> ...tab1 content... </div>
# <div class="tab-unit" id="tab2"> ...tab2 content... </div>
# </div>
#
# <script> $(".tabs-container").cstmTabs({ selected: ".default-tab", hover: true }); </script>
#
# ====================================== 

(($, window, document) ->
  Plugin = (element, options) ->
    @element = element
    @options = $.extend({}, defaults, options)
    @_defaults = defaults
    @_name = pluginName
    @init()
    return
  pluginName = "cstmTabs2"
  defaults =
    selected: 0
    hover: false

  Plugin::init = ->
    tabsContainer = $(@element)
    tabUnits = tabsContainer.find(".tab-unit")
    tabLinks = tabsContainer.find(".tab-link")
    selected = (if @options.selected then @options.selected else tabLinks.eq(0)[0])
    eventType = (if @options.hover then "mouseenter" else "click")
    tabUnits.hide()
    tabsContainer.on eventType + ".tabs", ".tab-link", (e) ->
      e.preventDefault()
      tabUnits.filter(":visible").removeClass("active-tab").hide()
      tabLinks.removeClass "active-link"
      $(@hash).show().addClass "active-tab"
      $(this).addClass "active-link"
      return

    tabsContainer.find(selected).trigger eventType + ".tabs"
    return

  $.fn[pluginName] = (options) ->
    @each ->
      $.data this, "plugin_" + pluginName, new Plugin(this, options)  unless $.data(this, "plugin_" + pluginName)
      return


  return
) jQuery, window, document







